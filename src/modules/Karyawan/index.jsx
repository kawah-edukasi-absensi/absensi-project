import React from "react";
import { useCurrentRoutes } from "@/utils/hooks";
import { karyawan } from "@/mocks/fakeData";
import { useQuery } from "@tanstack/react-query";
import { ROUTE } from "@/lib/Routes";
import * as COMPONENT from "@/components";
import * as QUERY from "@/API/Karyawan/Queries";
import * as MISC from "@/misc";

export default function Karyawan() {
  const { isLoading } = useQuery(QUERY.karyawanMainQuery());
  const routes = useCurrentRoutes();

  if (isLoading) return <MISC.LoadingPage />;

  return (
    <COMPONENT.PageContainer>
      <div className="w-full flex flex-col">
        <COMPONENT.RoutesBread routes={routes} routesName={"Dashboard"} />
        <COMPONENT.Table
          dataTable={karyawan}
          muatUlang={ROUTE.KARYAWAN}
          create={ROUTE.CREATE_KARYAWAN}
        />
      </div>
    </COMPONENT.PageContainer>
  );
}
