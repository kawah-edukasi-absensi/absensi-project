import React, { useRef, useState } from "react";
import { useNavigate } from "react-router-dom";
import { useCurrentRoutes } from "@/utils/hooks";
import * as COMPONENT from "@/components";

export default function CreateKaryawan() {
  const navigate = useNavigate();
  const routes = useCurrentRoutes();
  const formRef = useRef(null);
  const [formData, setFormData] = useState({});
  const [formErrors, setFormErrors] = useState({});
  const [selectValueProvinsi, setSelectValueProvinsi] = useState("");
  const [selectValueKotaKabupaten, setSelectValueKotaKabupaten] = useState("");
  const [selectValueKecamatan, setSelectValueKecamatan] = useState("");
  const [selectValueKelurahanDesa, setSelectValueKelurahanDesa] = useState("");

  const optionsProvinsi = [
    { label: "Pilih Salah Satu", value: "" },
    { label: "Jawa Barat", value: "Jawa Barat" },
    { label: "DKI Jakarta", value: "DKI Jakarta" },
  ];
  const optionsKotaKabupaten = [
    { label: "Pilih Salah Satu", value: "" },
    { label: "Kota Bekasi", value: "Kota Bekasi" },
    { label: "Jakarta Utara", value: "Jakarta Utara" },
  ];
  const optionsKecamatan = [
    { label: "Pilih Salah Satu", value: "" },
    { label: "Bekasi Utara", value: "Bekasi Utara" },
    { label: "Pademangan", value: "Pademangan" },
  ];
  const optionsKelurahanDesa = [
    { label: "Pilih Salah Satu", value: "" },
    { label: "Harapan Baru", value: "Harapan Baru" },
    { label: "Pademangan Timur", value: "Pademangan Timur" },
  ];

  const handleInputChange = (event) => {
    const { name, value } = event.target;

    name === "provinsi" && setSelectValueProvinsi(value);
    name === "kotaKabupaten" && setSelectValueKotaKabupaten(value);
    name === "kecamatan" && setSelectValueKecamatan(value);
    name === "kelurahanDesa" && setSelectValueKelurahanDesa(value);
    setFormData({ ...formData, [name]: value });
    setFormErrors({ ...formErrors, [name]: "" });
  };

  const validateForm = () => {
    const errors = {};
    let isValid = true;

    if (!formData.namaLengkap) {
      errors.namaLengkap = "Nama Lengkap Wajib diisi";
      isValid = false;
    }
    if (!formData.tempatLahir) {
      errors.tempatLahir = "Tempat Lahir Wajib diisi";
      isValid = false;
    }
    if (!formData.tanggalLahir) {
      errors.tanggalLahir = "Tanggal Lahir Wajib diisi";
      isValid = false;
    }
    if (!formData.jenisKelamin) {
      errors.jenisKelamin = "Jenis Kelamin Wajib diisi";
      isValid = false;
    }
    if (!formData.agama) {
      errors.agama = "Agama Wajib diisi";
      isValid = false;
    }
    if (!formData.noHp) {
      errors.noHp = "Nomor HP Wajib diisi";
      isValid = false;
    } else if (formData.noHp.length < 10 || formData.noHp.length > 12) {
      errors.noHp = "Nomor HP Harus terdiri dari 10 sampai 12 digit";
      isValid = false;
    }
    if (!formData.pendidikanTerakhir) {
      errors.pendidikanTerakhir = "Pendidikan Terakhir Wajib diisi";
      isValid = false;
    }
    if (!formData.email) {
      errors.email = "Email Wajib diisi";
      isValid = false;
    } else if (
      !/^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/.test(formData.email)
    ) {
      errors.email = "Format Email Salah";
      isValid = false;
    }
    if (!formData.tanggalGabung) {
      errors.tanggalGabung = "Tanggal Gabung Perusahaan Wajib diisi";
      isValid = false;
    }
    if (!formData.alamat) {
      errors.alamat = "Alamat Tempat Tinggal Wajib diisi";
      isValid = false;
    }
    if (!formData.provinsi) {
      errors.provinsi = "Provinsi Wajib diisi";
      isValid = false;
    }
    if (!formData.kotaKabupaten) {
      errors.kotaKabupaten = "Kota / Kabupaten Wajib diisi";
      isValid = false;
    }
    if (!formData.kecamatan) {
      errors.kecamatan = "Kecamatan Wajib diisi";
      isValid = false;
    }
    if (!formData.kelurahanDesa) {
      errors.kelurahanDesa = "Kelurahan / Desa Wajib diisi";
      isValid = false;
    }
    if (!formData.kodePos) {
      errors.kodePos = "Kode Pos Wajib diisi";
      isValid = false;
    } else if (formData.kodePos.length < 5) {
      errors.kodePos = "Kode Pos Harus terdiri dari 5 digit";
      isValid = false;
    }
    if (!formData.noKtp) {
      errors.noKtp = "Nomor KTP Wajib diisi";
      isValid = false;
    } else if (
      !/^(1[1-9]|21|[37][1-6]|5[1-3]|6[1-5]|[89][12])\d{2}\d{2}([04][1-9]|[1256][0-9]|[37][01])(0[1-9]|1[0-2])\d{2}\d{4}$/.test(
        formData.noKtp
      )
    ) {
      errors.noKtp = "Nomor KTP Harus terdiri dari 16 digit";
      isValid = false;
    }
    if (!formData.uploadKtp) {
      errors.uploadKtp = "Upload KTP Wajib diisi";
      isValid = false;
    }

    setFormErrors(errors);
    return isValid;
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    const isValid = validateForm();
    if (isValid) {
      console.log(formData);
      setFormData({});
      setFormErrors({});
      setSelectValueProvinsi("");
      setSelectValueKotaKabupaten("");
      setSelectValueKecamatan("");
      setSelectValueKelurahanDesa("");
      setTimeout(() => {
        formRef.current.reset();
      }, 0);
    }
  };

  return (
    <COMPONENT.PageContainer>
      <COMPONENT.RoutesBread routes={routes} routesName={"Dashboard Create"} />
      <COMPONENT.CreateHeader navigate={() => navigate(-1)} />
      <div className="w-full rounded-md bg-white p-6">
        <form ref={formRef} onSubmit={handleSubmit}>
          <div className="grid grid-cols-1 gap-y-4 gap-x-10 md:grid-cols-2 md:justify-center md:items-center">
            <div className="w-full flex flex-col md:row-start-1 md:row-end-2">
              <label htmlFor="namaLengkap" className="w-fit mb-1">
                Nama Lengkap
                <span className="text-red-600 font-bold">*</span>
              </label>
              {formErrors.namaLengkap && (
                <span className="text-red-600 text-sm font-semibold">
                  {formErrors.namaLengkap}
                </span>
              )}
              <input
                type="text"
                autoComplete="nope"
                name="namaLengkap"
                id="namaLengkap"
                placeholder="Nama Lengkap"
                onChange={handleInputChange}
                className="border p-3 rounded"
              />
            </div>
            <div className="w-full flex gap-4 md:flex-nowrap flex-wrap md:gap-5 md:row-start-2 md:row-end-3">
              <div className="w-full flex flex-col">
                <label htmlFor="tempatLahir" className="w-fit mb-1">
                  Tempat Lahir
                  <span className="text-red-600 font-bold">*</span>
                </label>
                {formErrors.tempatLahir && (
                  <span className="text-red-600 text-sm font-semibold">
                    {formErrors.tempatLahir}
                  </span>
                )}
                <input
                  type="text"
                  autoComplete="nope"
                  name="tempatLahir"
                  id="tempatLahir"
                  placeholder="Tempat Lahir"
                  onChange={handleInputChange}
                  className="w-full border p-3 rounded"
                />
              </div>
              <div className="flex flex-col">
                <label htmlFor="tanggalLahir" className="w-fit mb-1">
                  Tanggal<span className="text-red-600 font-bold">*</span>
                </label>
                {formErrors.tanggalLahir && (
                  <span className="text-red-600 text-sm font-semibold">
                    {formErrors.tanggalLahir}
                  </span>
                )}
                <input
                  type="date"
                  name="tanggalLahir"
                  id="tanggalLahir"
                  onChange={handleInputChange}
                  className="border p-3 rounded bg-white"
                />
              </div>
            </div>
            <div className="w-full flex flex-col md:row-start-3 md:row-end-4">
              <label htmlFor="lakiLaki" className="w-fit mb-1">
                Jenis Kelamin
                <span className="text-red-600 font-bold">*</span>
              </label>
              {formErrors.jenisKelamin && (
                <span className="text-red-600 text-sm font-semibold">
                  {formErrors.jenisKelamin}
                </span>
              )}
              <div className="flex gap-6">
                <div className="flex items-center gap-2">
                  <input
                    type="radio"
                    name="jenisKelamin"
                    id="lakiLaki"
                    value="Laki - laki"
                    checked={formData.jenisKelamin === "Laki - laki"}
                    onChange={handleInputChange}
                    className="w-fit border p-3 rounded"
                  />
                  <label htmlFor="lakiLaki">Laki - laki</label>
                </div>
                <div className="flex items-center gap-2">
                  <input
                    type="radio"
                    name="jenisKelamin"
                    id="perempuan"
                    value="Perempuan"
                    checked={formData.jenisKelamin === "Perempuan"}
                    onChange={handleInputChange}
                    className="w-fit border p-3 rounded"
                  />
                  <label htmlFor="perempuan">Perempuan</label>
                </div>
              </div>
            </div>
            <div className="w-full flex flex-col md:row-start-4 md:row-end-5">
              <label htmlFor="agama" className="w-fit mb-1">
                Agama<span className="text-red-600 font-bold">*</span>
              </label>
              {formErrors.agama && (
                <span className="text-red-600 text-sm font-semibold">
                  {formErrors.agama}
                </span>
              )}
              <input
                type="text"
                autoComplete="nope"
                name="agama"
                id="agama"
                placeholder="Agama"
                onChange={handleInputChange}
                className="border p-3 rounded"
              />
            </div>
            <div className="w-full flex flex-col md:row-start-5 md:row-end-6">
              <label htmlFor="noHp" className="w-fit mb-1">
                Nomor HP/Kontak
                <span className="text-red-600 font-bold">*</span>
              </label>
              {formErrors.noHp && (
                <span className="text-red-600 text-sm font-semibold">
                  {formErrors.noHp}
                </span>
              )}
              <input
                type="number"
                autoComplete="nope"
                name="noHp"
                id="noHp"
                placeholder="Nomor HP/Kontak"
                onChange={handleInputChange}
                className="border p-3 rounded"
              />
            </div>
            <div className="w-full flex flex-col md:row-start-6 md:row-end-7">
              <label htmlFor="noKontakDarurat" className="w-fit mb-1">
                Nomor Kontak Darurat
              </label>
              <input
                type="number"
                autoComplete="nope"
                name="noKontakDarurat"
                id="noKontakDarurat"
                placeholder="Nomor Kontak Darurat"
                onChange={handleInputChange}
                className="border p-3 rounded"
              />
            </div>
            <div className="w-full flex flex-col md:row-start-7 md:row-end-[8]">
              <label htmlFor="pendidikanTerakhir" className="w-fit mb-1">
                Pendidikan Terakhir
                <span className="text-red-600 font-bold">*</span>
              </label>
              {formErrors.pendidikanTerakhir && (
                <span className="text-red-600 text-sm font-semibold">
                  {formErrors.pendidikanTerakhir}
                </span>
              )}
              <input
                type="text"
                autoComplete="nope"
                name="pendidikanTerakhir"
                id="pendidikanTerakhir"
                placeholder="Pendidikan Terakhir"
                onChange={handleInputChange}
                className="border p-3 rounded"
              />
            </div>

            <div className="w-full flex flex-col md:row-start-[8] md:row-end-[9]">
              <label htmlFor="email" className="w-fit mb-1">
                Alamat Email
                <span className="text-red-600 font-bold">*</span>
              </label>
              {formErrors.email && (
                <span className="text-red-600 text-sm font-semibold">
                  {formErrors.email}
                </span>
              )}
              <input
                type="email"
                autoComplete="nope"
                name="email"
                id="email"
                placeholder="Alamat Email"
                onChange={handleInputChange}
                className="border p-3 rounded"
              />
            </div>
            <div className="w-full flex flex-col md:row-start-[9] md:row-end-[10]">
              <label htmlFor="tanggalGabung" className="w-fit mb-1">
                Tanggal bergabung dengan perusahaan
                <span className="text-red-600 font-bold">*</span>
              </label>
              {formErrors.tanggalGabung && (
                <span className="text-red-600 text-sm font-semibold">
                  {formErrors.tanggalGabung}
                </span>
              )}
              <input
                type="date"
                name="tanggalGabung"
                id="tanggalGabung"
                onChange={handleInputChange}
                className="md:w-fit border p-3 rounded bg-white"
              />
            </div>
            <div className="w-full flex flex-col md:row-start-[10] md:row-end-[11]">
              <label htmlFor="alamat" className="w-[85%] mb-1">
                Alamat Tempat Tinggal Sesuai dengan KTP
                <span className="text-red-600 font-bold">*</span>
              </label>
              {formErrors.alamat && (
                <span className="text-red-600 text-sm font-semibold">
                  {formErrors.alamat}
                </span>
              )}
              <textarea
                autoComplete="nope"
                name="alamat"
                id="alamat"
                placeholder="Alamat Tempat Tinggal Sesuai dengan KTP"
                cols="30"
                rows="5"
                onChange={handleInputChange}
                className="border p-3 rounded"
              />
            </div>
            <COMPONENT.SelectInputProvinsi
              id="provinsi"
              name="Provinsi"
              value={selectValueProvinsi}
              onChange={handleInputChange}
              options={optionsProvinsi}
              error={formErrors.provinsi}
            />
            <COMPONENT.SelectInputKotaKabupaten
              id="kotaKabupaten"
              name="Kota / Kabupaten"
              value={selectValueKotaKabupaten}
              onChange={handleInputChange}
              options={optionsKotaKabupaten}
              error={formErrors.kotaKabupaten}
            />
            <COMPONENT.SelectInputKecamatan
              id="kecamatan"
              name="Kecamatan"
              value={selectValueKecamatan}
              onChange={handleInputChange}
              options={optionsKecamatan}
              error={formErrors.kecamatan}
            />
            <COMPONENT.SelectInputKelurahanDesa
              id="kelurahanDesa"
              name="Kelurahan / Desa"
              value={selectValueKelurahanDesa}
              onChange={handleInputChange}
              options={optionsKelurahanDesa}
              error={formErrors.kelurahanDesa}
            />
            <div className="w-full flex flex-col md:row-start-5 md:row-end-6">
              <label htmlFor="kodePos" className="w-fit mb-1">
                Kode Pos<span className="text-red-600 font-bold">*</span>
              </label>
              {formErrors.kodePos && (
                <span className="text-red-600 text-sm font-semibold">
                  {formErrors.kodePos}
                </span>
              )}
              <input
                type="number"
                autoComplete="nope"
                name="kodePos"
                id="kodePos"
                placeholder="Kode Pos"
                onChange={handleInputChange}
                className="w-fit border p-3 rounded"
              />
            </div>
            <div className="w-full flex flex-col md:row-start-6 md:row-end-7">
              <label htmlFor="noKtp" className="w-fit mb-1">
                Nomor KTP<span className="text-red-600 font-bold">*</span>
              </label>
              {formErrors.noKtp && (
                <span className="text-red-600 text-sm font-semibold">
                  {formErrors.noKtp}
                </span>
              )}
              <input
                type="number"
                autoComplete="nope"
                name="noKtp"
                id="noKtp"
                placeholder="Nomor KTP"
                onChange={handleInputChange}
                className="w-fit border p-3 rounded"
              />
            </div>
            <div className="w-full flex flex-col md:row-start-7 md:row-end-[8]">
              <label htmlFor="uploadKtp" className="w-fit mb-1">
                Upload KTP<span className="text-red-600 font-bold">*</span>
              </label>
              {formErrors.uploadKtp && (
                <span className="text-red-600 text-sm font-semibold">
                  {formErrors.uploadKtp}
                </span>
              )}
              <input
                type="file"
                name="uploadKtp"
                id="uploadKtp"
                onChange={handleInputChange}
                accept="image/jpg, image/jpeg, image/png"
                className="md:w-fit border p-3 rounded"
              />
            </div>
          </div>
          <COMPONENT.ButtonForm />
        </form>
      </div>
    </COMPONENT.PageContainer>
  );
}
