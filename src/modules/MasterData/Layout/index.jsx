import React from "react";
import * as COMPONENT from "@/components";
import { Outlet, useNavigate } from "react-router-dom";
import { useCurrentRoutes } from "@/utils/hooks";
import { NavLink } from "react-router-dom";
import { ROUTE } from "@/lib/Routes";
import { BiCalendarEvent, BiTimer, BiTime, BiCalendar } from "react-icons/bi";

import clsx from "clsx";

export default function MasterDataLayout() {
  const masterDataItems = [
    {
      name: "Hari Libur Nasional",
      path: ROUTE.MASTER_DATA.HARI_LIBUR_NASIONAL,
      icon: <BiCalendarEvent />,
    },
    {
      name: "Cuti Tahunan",
      path: ROUTE.MASTER_DATA.CUTI_TAHUNAN,
      icon: <BiCalendar />,
    },
    {
      name: "Toleransi Keterlambatan",
      path: ROUTE.MASTER_DATA.TOLERANSI_KETERLAMBATAN,
      icon: <BiTimer />,
    },
    {
      name: "Jam Kerja",
      path: ROUTE.MASTER_DATA.JAM_KERJA,
      icon: <BiTime />,
    },
  ];

  const linkActiveClass =
    "rounded-md text-white bg-indigo-500 shadow-indigo-300 shadow-md";
  const linkInactiveClass = "rounded-md text-slate-500";

  const navigate = useNavigate();
  const routes = useCurrentRoutes();

  return (
    <COMPONENT.PageContainer>
      <div className="w-full h-auto flex flex-col">
        <div
          id="layout-head"
          className={`${
            routes[2] ? "py-3 mb-4" : "min-h-[8rem] gap-4 pt-3"
          } flex flex-col justify-end`}
        >
          <h1 className="md:text-2xl text-lg font-bold text-slate-600">
            <span className="text-slate-400 font-normal">
              {routes[0].name} /{" "}
            </span>
            <span
              className={`${
                routes[2]
                  ? "text-slate-400 font-normal"
                  : "text-slate-600 font-bold"
              } `}
            >
              {routes[1] && `${routes[1].name}`} {routes[1] && ""}{" "}
              {routes[2] && `/ `}
            </span>
            {routes[2] && `${routes[2].name}`}
          </h1>
          {routes[2] ? null : (
            <div className="w-full flex items-center md:gap-4 gap-2 overflow-x-auto flex-nowrap py-3 mb-4">
              {masterDataItems.map((item) => (
                <NavLink key={item.name} to={item.path}>
                  {({ isActive }) => (
                    <div
                      className={clsx(
                        "text-sm font-medium md:px-5 md:py-2.5 px-3 py-2 whitespace-nowrap flex items-center md:gap-3 gap-2",
                        isActive ? linkActiveClass : linkInactiveClass
                      )}
                    >
                      <div className="text-normal">{item.icon}</div>
                      <div>{item.name}</div>
                    </div>
                  )}
                </NavLink>
              ))}
            </div>
          )}
        </div>
        {routes[2] && <COMPONENT.CreateHeader navigate={() => navigate(-1)} />}
        <Outlet />
      </div>
    </COMPONENT.PageContainer>
  );
}
