import React from "react";
import { hariLiburNasional } from "@/mocks/fakeData";
import { ROUTE } from "@/lib/Routes";
import * as COMPONENT from "@/components";

export default function ToleransiKeterlambatan() {
  return (
    <div className="w-full flex flex-col">
      <COMPONENT.Table
        dataTable={hariLiburNasional}
        muatUlang={ROUTE.MASTER_DATA.TOLERANSI_KETERLAMBATAN}
        create={ROUTE.MASTER_DATA.CREATE_TOLERANSI_KETERLAMBATAN}
      />
    </div>
  );
}
