import React, { useRef, useState } from "react";

export default function CreateToleransiKeterlambatan() {
  const formRef = useRef(null);
  const [formData, setFormData] = useState({});
  const [formErrors, setFormErrors] = useState({});

  const handleInputChange = (event) => {
    const { name, value } = event.target;

    setFormData({ ...formData, [name]: value });
    setFormErrors({ ...formErrors, [name]: "" });
  };

  const validateForm = () => {
    const errors = {};
    let isValid = true;

    if (!formData.toleransiKeterlambatan) {
      errors.toleransiKeterlambatan = "Toleransi Keterlambatan wajib diisi";
      isValid = false;
    }

    setFormErrors(errors);
    return isValid;
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    const isValid = validateForm();
    if (isValid) {
      console.log(formData);
      setFormData({});
      setFormErrors({});
      setTimeout(() => {
        formRef.current.reset();
      }, 0);
    }
  };

  return (
    <div className="w-full rounded-lg bg-white p-6">
      <form
        ref={formRef}
        onSubmit={handleSubmit}
        className="w-full flex flex-col gap-4 max-w-[300px]"
      >
        <div>
          <label
            for="toleransiKeterlambatan"
            className="block mb-2 text-sm font-mediu"
          >
            Toleransi Keterlambatan (menit)
            <span className="text-red-500">*</span>
          </label>
          {formErrors.toleransiKeterlambatan && (
            <span className="text-red-600 text-sm font-semibold">
              {formErrors.toleransiKeterlambatan}
            </span>
          )}
          <input
            type="number"
            autoComplete="nope"
            name="toleransiKeterlambatan"
            id="toleransiKeterlambatan"
            placeholder="Toleransi Keterlambatan (menit)"
            onChange={handleInputChange}
            className="w-full border p-3 rounded"
          />
        </div>
        <button className="bg-green-500 hover:bg-green-400 font-medium rounded-lg text-sm text-white py-3">
          Simpan
        </button>
      </form>
    </div>
  );
}
