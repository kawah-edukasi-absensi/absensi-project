import React from "react";
import { hariLiburNasional } from "@/mocks/fakeData";
import { ROUTE } from "@/lib/Routes";
import * as COMPONENT from "@/components";

export default function HariLiburNasional() {
  return (
    <div className="w-full flex flex-col">
      <COMPONENT.Table
        dataTable={hariLiburNasional}
        muatUlang={ROUTE.MASTER_DATA.HARI_LIBUR_NASIONAL}
        create={ROUTE.MASTER_DATA.CREATE_HARI_LIBUR_NASIONAL}
      />
    </div>
  );
}
