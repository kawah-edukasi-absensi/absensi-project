import React, { useRef, useState } from "react";
import * as COMPONENT from "@/components";

export default function CreateHariLiburNasional() {
  const formRef = useRef(null);
  const [formData, setFormData] = useState({});
  const [formErrors, setFormErrors] = useState({});

  const handleInputChange = (event) => {
    const { name, value } = event.target;

    setFormData({ ...formData, [name]: value });
    setFormErrors({ ...formErrors, [name]: "" });
  };

  const validateForm = () => {
    const errors = {};
    let isValid = true;

    if (!formData.namaHariLibur) {
      errors.namaHariLibur = "Nama Hari Libur wajib diisi";
      isValid = false;
    }
    if (!formData.tanggalLibur) {
      errors.tanggalLibur = "Tanggal Libur wajib diisi";
      isValid = false;
    }

    setFormErrors(errors);
    return isValid;
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    const isValid = validateForm();
    if (isValid) {
      console.log(formData);
      setFormData({});
      setFormErrors({});
      setTimeout(() => {
        formRef.current.reset();
      }, 0);
    }
  };

  return (
    <div className="w-full rounded-md bg-white p-6">
      <form
        ref={formRef}
        onSubmit={handleSubmit}
        className="flex flex-col gap-5"
      >
        <div className="flex flex-col">
          <label htmlFor="namaHariLibur" className="w-fit mb-1">
            Nama Hari Libur<span className="text-red-600 font-bold">*</span>
          </label>
          {formErrors.namaHariLibur && (
            <span className="text-red-600 text-sm font-semibold">
              {formErrors.namaHariLibur}
            </span>
          )}
          <input
            type="text"
            autoComplete="nope"
            name="namaHariLibur"
            id="namaHariLibur"
            placeholder="Nama Hari Libur"
            onChange={handleInputChange}
            className="md:w-1/2 border p-3 rounded"
          />
        </div>
        <div className="w-fit flex flex-col">
          <label htmlFor="tanggalLibur" className="w-fit mb-1">
            Tanggal Libur<span className="text-red-600 font-bold">*</span>
          </label>
          {formErrors.tanggalLibur && (
            <span className="text-red-600 text-sm font-semibold">
              {formErrors.tanggalLibur}
            </span>
          )}
          <input
            type="date"
            name="tanggalLibur"
            id="tanggalLibur"
            onChange={handleInputChange}
            className="border p-3 rounded bg-white"
          />
        </div>
        <COMPONENT.ButtonForm />
      </form>
    </div>
  );
}
