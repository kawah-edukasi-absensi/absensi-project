import React from "react";
import { hariLiburNasional } from "@/mocks/fakeData";
import { ROUTE } from "@/lib/Routes";
import * as COMPONENT from "@/components";

export default function JamKerja() {
  return (
    <div className="w-full flex flex-col">
      <COMPONENT.Table
        dataTable={hariLiburNasional}
        muatUlang={ROUTE.MASTER_DATA.JAM_KERJA}
        create={ROUTE.MASTER_DATA.CREATE_JAM_KERJA}
      />
    </div>
  );
}
