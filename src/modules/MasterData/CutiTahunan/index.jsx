import React from "react";
import { hariLiburNasional } from "@/mocks/fakeData";
import { ROUTE } from "@/lib/Routes";
import * as COMPONENT from "@/components";

export default function CutiTahunan() {
  return (
    <div className="w-full flex flex-col">
      <COMPONENT.Table
        dataTable={hariLiburNasional}
        muatUlang={ROUTE.MASTER_DATA.CUTI_TAHUNAN}
        create={ROUTE.MASTER_DATA.CREATE_CUTI_TAHUNAN}
      />
    </div>
  );
}
