import React, { useEffect, useState } from "react";
import { BiQrScan } from "react-icons/bi";
import { QrReader } from "react-qr-reader";
import { useCurrentRoutes } from "@/utils/hooks";
import * as COMPONENT from "@/components";

export default function WorkFromOffice() {
  const routes = useCurrentRoutes();
  const [showCam, setShowCam] = useState(false);
  const [scanResult, setScanResult] = useState("");
  const [location, setLocation] = useState(null);
  const [latitude, setLatitude] = useState(null);
  const [longitude, setLongitude] = useState(null);

  const isTouchScreenDevice = () => {
    try {
      document.createEvent("TouchEvent");
      return true;
    } catch (e) {
      return false;
    }
  };

  const handleStartScan = () => {
    setShowCam(true);
  };

  const handleScanQr = (result, error) => {
    if (!!result) {
      setScanResult(result?.text);
    }
  };

  let geocode = {
    reverseGeocode: function (latitude, longitude) {
      var api_key = "d320c66decb8409ab91caeba40cdc3be";
      var api_url = "https://api.opencagedata.com/geocode/v1/json";

      var request_url =
        api_url +
        "?" +
        "key=" +
        api_key +
        "&q=" +
        encodeURIComponent(latitude + "," + longitude) +
        "&pretty=1" +
        "&no_annotations=1";

      var request = new XMLHttpRequest();
      request.open("GET", request_url, true);

      request.onload = function () {
        if (request.status === 200) {
          var data = JSON.parse(request.responseText);
          setLocation(
            data.results[0].components.road !== "unnamed road"
              ? data.results[0].components.road +
                  ", " +
                  data.results[0].components.village +
                  ", " +
                  data.results[0].components.city +
                  " " +
                  data.results[0].components.postcode +
                  ", " +
                  data.results[0].components.state +
                  ", " +
                  data.results[0].components.country +
                  "."
              : data.results[0].components.village +
                  ", " +
                  data.results[0].components.city +
                  " " +
                  data.results[0].components.postcode +
                  ", " +
                  data.results[0].components.state +
                  ", " +
                  data.results[0].components.country
          );
          setLatitude(data.results[0].geometry.lat);
          setLongitude(data.results[0].geometry.lng);
        } else if (request.status <= 500) {
          console.log("unable to geocode! Response code: " + request.status);
          var dataErr = JSON.parse(request.responseText);
          console.log("error msg: " + dataErr.status.message);
        } else {
          console.log("server error");
        }
      };

      request.onerror = function () {
        console.log("unable to connect to server");
      };

      request.send();
    },
    getLocation: function () {
      function success(data) {
        geocode.reverseGeocode(data.coords.latitude, data.coords.longitude);
      }
      if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(success, console.error);
      }
    },
  };

  useEffect(() => {
    geocode.getLocation();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <COMPONENT.PageContainer>
      <COMPONENT.RoutesBread routes={routes} routesName={"Absensi"} />
      {!isTouchScreenDevice() ? (
        <div className="w-full h-[calc(100vh-194.51px)] bg-white rounded-md flex flex-col gap-12 p-6 justify-center items-center text-center">
          <h2 className="text-2xl font-bold">
            Scan Barcode di Mobile Device Anda!
          </h2>
          <BiQrScan className="text-9xl" />
        </div>
      ) : (
        <div className="w-full flex flex-col justify-start rounded-md bg-white p-6">
          <h1 className="text-center text-2xl text-slate-600 font-bold md:text-start ">
            Scan Barcode
          </h1>
          <div className="flex flex-col gap-5">
            {!showCam ? (
              <div className="relative flex justify-center items-center mt-5">
                <BiQrScan className="text-9xl" />
                <button
                  onClick={handleStartScan}
                  className="absolute py-2.5 px-3 bg-indigo-500 rounded-md text-white text-sm font-semibold"
                >
                  SCAN
                </button>
              </div>
            ) : (
              <div className="md:w-3/12">
                <QrReader
                  constraints={{
                    facingMode: "environment",
                  }}
                  scanDelay={300}
                  onResult={handleScanQr}
                  style={{ width: "100%" }}
                  className="qr-image-wrapper py-4"
                />
                <h2 className="text-lg font-semibold">
                  Scan Result: {scanResult}
                </h2>
              </div>
            )}
            <div className="flex flex-col">
              <h2 className="text-xl font-semibold mb-2">Lokasi</h2>
              <p id="statusLocation" className="text-lg">
                {location}
              </p>
              <div className="flex gap-4 mt-2">
                <p>Latitude : {latitude}°</p>
                <p>Longitude : {longitude}°</p>
              </div>
            </div>
          </div>
        </div>
      )}
    </COMPONENT.PageContainer>
  );
}
