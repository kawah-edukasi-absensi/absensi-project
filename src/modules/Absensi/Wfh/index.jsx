import React, { useEffect, useRef, useState } from "react";
import {
  BiCamera,
  BiCheckCircle,
  BiUserCheck,
  BiUserX,
  BiXCircle,
} from "react-icons/bi";
import { useCurrentRoutes } from "@/utils/hooks";
import * as COMPONENT from "@/components";
import useTakePicture from "@/utils/hooks/useTakePicture";
import clsx from "clsx";

export default function WorkFromHome() {
  const routes = useCurrentRoutes();
  const {
    videoRef,
    photoRef,
    dataURL,
    setDataURL,
    showCamera,
    getVideo,
    takePhoto,
    savePhoto,
    closePhoto,
  } = useTakePicture();
  const [showContent, setShowContent] = useState(false);
  const [isFirstClick, setIsFirstClick] = useState(true);
  const [location, setLocation] = useState(null);
  const [latitude, setLatitude] = useState(null);
  const [longitude, setLongitude] = useState(null);
  const [tanggalWaktu, setTanggalWaktu] = useState("");
  const [formValues, setFormValues] = useState({
    rencanaKerja: "",
    photoSrc: "",
  });
  const [formError, setFormError] = useState({
    rencanaKerja: "",
    photoSrc: "",
  });
  const formRef = useRef();

  const linkActiveClass =
    "rounded-md text-white bg-indigo-500 shadow-indigo-300 shadow-md";
  const linkInactiveClass = "rounded-md text-slate-500";

  useEffect(() => {
    setFormValues((prevFormValues) => ({
      ...prevFormValues,
      photoSrc: dataURL,
    }));
  }, [dataURL]);

  const handleSubmit = (e) => {
    e.preventDefault();

    setFormError((prev) => ({
      ...prev,
      photoSrc: "",
    }));

    setFormValues({
      ...formValues,
      photoSrc: dataURL,
    });

    if ((!formValues.rencanaKerja || !dataURL) && isFirstClick) {
      const errors = {};

      if (!showContent) {
        if (!formValues.rencanaKerja) {
          errors.rencanaKerja = "Rencana Kerja wajib diisi";
        }
        if (!dataURL) {
          errors.photoSrc = "Photo wajib diisi";
        }
      } else {
        if (!dataURL) {
          errors.photoSrc = "Photo wajib diisi";
        }
      }

      if (errors.rencanaKerja || errors.photoSrc) {
        setFormError(errors);
        setIsFirstClick(false);
        return;
      }
    }

    if (!showContent) {
      console.log("Absen Masuk Berhasil : ", {
        location,
        tanggalWaktu,
        ...formValues,
      });
    } else {
      console.log("Absen Keluar Berhasil : ", {
        location,
        tanggalWaktu,
        photoSrc: formValues.photoSrc,
      });
    }
    setFormValues({});
    setDataURL("");
    setTimeout(() => {
      formRef.current.reset();
    }, 0);
  };

  const dateTimeNow = () => {
    let dateTime = new Date();
    dateTime.setMinutes(dateTime.getMinutes() - dateTime.getTimezoneOffset());
    let tanggalWaktu = (document.getElementById("tanggalWaktu").value = dateTime
      .toISOString()
      .slice(0, 16));
    setTanggalWaktu(tanggalWaktu);
  };

  let geocode = {
    reverseGeocode: function (latitude, longitude) {
      var api_key = "d320c66decb8409ab91caeba40cdc3be";
      var api_url = "https://api.opencagedata.com/geocode/v1/json";

      var request_url =
        api_url +
        "?" +
        "key=" +
        api_key +
        "&q=" +
        encodeURIComponent(latitude + "," + longitude) +
        "&pretty=1" +
        "&no_annotations=1";

      var request = new XMLHttpRequest();
      request.open("GET", request_url, true);

      request.onload = function () {
        if (request.status === 200) {
          var data = JSON.parse(request.responseText);
          setLocation(
            data.results[0].components.road !== "unnamed road"
              ? data.results[0].components.road +
                  ", " +
                  data.results[0].components.village +
                  ", " +
                  data.results[0].components.city +
                  " " +
                  data.results[0].components.postcode +
                  ", " +
                  data.results[0].components.state +
                  ", " +
                  data.results[0].components.country +
                  "."
              : data.results[0].components.village +
                  ", " +
                  data.results[0].components.city +
                  " " +
                  data.results[0].components.postcode +
                  ", " +
                  data.results[0].components.state +
                  ", " +
                  data.results[0].components.country
          );
          setLatitude(data.results[0].geometry.lat);
          setLongitude(data.results[0].geometry.lng);
        } else if (request.status <= 500) {
          console.log("unable to geocode! Response code: " + request.status);
          var dataErr = JSON.parse(request.responseText);
          console.log("error msg: " + dataErr.status.message);
        } else {
          console.log("server error");
        }
      };

      request.onerror = function () {
        console.log("unable to connect to server");
      };

      request.send();
    },
    getLocation: function () {
      function success(data) {
        geocode.reverseGeocode(data.coords.latitude, data.coords.longitude);
      }
      if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(success, console.error);
      }
    },
  };

  useEffect(() => {
    const interval = setInterval(() => {
      dateTimeNow();
    }, 1000);

    geocode.getLocation();

    return () => clearInterval(interval);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <COMPONENT.PageContainer>
      <COMPONENT.RoutesBread routes={routes} routesName={"Absensi"} />
      <div className="w-full rounded-md bg-white p-6">
        <COMPONENT.CreateHeader name="Remote" />
        <div className="overflow-x-auto flex md:gap-4 gap-2 items-center justify-start mb-4 pb-3">
          <button
            onClick={() => setShowContent(false)}
            className={clsx(
              "text-sm font-medium md:px-5 md:py-2.5 px-3 py-2 whitespace-nowrap flex items-center md:gap-3 gap-2",
              !showContent ? linkActiveClass : linkInactiveClass
            )}
          >
            <BiUserCheck className="text-base" />
            Absen Masuk
          </button>
          <button
            onClick={() => {
              setIsFirstClick(true);
              setShowContent(true);
              setFormValues({
                ...formValues,
                rencanaKerja: "",
                photoSrc: "",
              });
              setDataURL("");
            }}
            className={clsx(
              "text-sm font-medium md:px-5 md:py-2.5 px-3 py-2 whitespace-nowrap flex items-center md:gap-3 gap-2",
              showContent ? linkActiveClass : linkInactiveClass
            )}
          >
            <BiUserX className="text-base" />
            Absen Keluar
          </button>
        </div>

        <form
          ref={formRef}
          onSubmit={handleSubmit}
          className="flex flex-col gap-5 mt-5"
        >
          <div className="flex flex-col">
            <h2 className="text-xl font-semibold mb-2">Lokasi</h2>
            <p id="statusLocation" className="text-lg">
              {location}
            </p>
            <div className="flex gap-4 mt-2">
              <p>Latitude : {latitude}°</p>
              <p>Longitude : {longitude}°</p>
            </div>
          </div>
          <div className="w-fit flex flex-col">
            <label htmlFor="tanggalWaktu" className="w-fit mb-1">
              Tanggal dan Waktu
              <span className="text-red-600 font-bold">*</span>
            </label>
            <input
              readOnly
              type="datetime-local"
              name="tanggalWaktu"
              id="tanggalWaktu"
              className="border p-3 rounded bg-white"
            />
          </div>
          {!showContent && (
            <div className="md:w-1/2 flex flex-col">
              <label htmlFor="rencanaKerja" className="w-fit mb-1">
                Rencana Kerja
                <span className="text-red-600 font-bold">*</span>
              </label>
              {formError.rencanaKerja && (
                <span className="text-red-600 text-sm font-semibold">
                  {formError.rencanaKerja}
                </span>
              )}
              <textarea
                autoComplete="nope"
                name="rencanaKerja"
                id="rencanaKerja"
                placeholder="Rencana Kerja"
                cols="30"
                rows="5"
                onChange={(e) => {
                  setFormError((prev) => ({
                    ...prev,
                    rencanaKerja: "",
                  }));
                  setFormValues({
                    ...formValues,
                    rencanaKerja: e.target.value,
                  });
                }}
                className="border p-3 rounded"
              />
            </div>
          )}
          <div className="flex flex-col">
            <p className="mb-1">
              Ambil Photo
              <span className="text-red-600 font-bold">*</span>
            </p>
            {formError.photoSrc && (
              <span className="text-red-600 text-sm font-semibold">
                {formError.photoSrc}
              </span>
            )}
            {showCamera && (
              <div className="w-fit relative mb-4">
                <video ref={videoRef} className="rounded-lg"></video>
                <button
                  type="button"
                  onClick={() => takePhoto()}
                  className="bg-indigo-500 hover:bg-indigo-400 text-white text-4xl p-2 rounded-full absolute bottom-0 left-1/2 -translate-x-1/2 mb-2"
                >
                  <BiCamera />
                </button>
                <canvas
                  ref={photoRef}
                  className="absolute top-0 right-0 rounded-lg w-1/3 h-auto"
                ></canvas>
                <div className="absolute bottom-0 right-0 flex gap-4 mb-2 mr-4">
                  <button
                    type="button"
                    onClick={() => {
                      savePhoto();
                      setFormError((prev) => ({
                        ...prev,
                        photoSrc: "",
                      }));
                    }}
                    className="bg-green-500 hover:bg-green-400 text-white md:text-4xl text-3xl rounded-full"
                  >
                    <BiCheckCircle />
                  </button>
                  <button
                    type="button"
                    onClick={closePhoto}
                    className="bg-red-500 hover:bg-red-400 text-white md:text-4xl text-3xl rounded-full"
                  >
                    <BiXCircle />
                  </button>
                </div>
              </div>
            )}
            {dataURL && (
              <img
                src={dataURL}
                alt="Absen Profile"
                className="md:w-1/3 rounded-lg mb-2"
              />
            )}
            <button
              type="button"
              onClick={() => {
                getVideo();
                setIsFirstClick(false);
              }}
              className="w-fit bg-slate-600 hover:bg-slate-500 px-5 py-2.5 rounded-md text-white"
            >
              Kamera
            </button>
          </div>
          <COMPONENT.ButtonForm name={"Absen Masuk"} />
        </form>
      </div>
    </COMPONENT.PageContainer>
  );
}
