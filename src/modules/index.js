export { default as Main } from "./Main";
export { default as AuthLogin } from "./Auth/Login";
export { default as WorkFromHome } from "./Absensi/Wfh";
export { default as WorkFromOffice } from "./Absensi/Wfo";
export { default as IjinCuti } from "./IjinCuti";
export { default as Karyawan } from "./Karyawan";
export { default as MasterDataLayout } from "./MasterData/Layout";
export { default as HariLiburNasional } from "./MasterData/HariLiburNasional";
export { default as CutiTahunan } from "./MasterData/CutiTahunan";
export { default as ToleransiKeterlambatan } from "./MasterData/ToleransiKeterlambatan";
export { default as JamKerja } from "./MasterData/JamKerja";
export { default as Pengguna } from "./Pengguna";

// CRUD SECTION
export { default as CreateIjinCuti } from "./IjinCuti/Create";
export { default as CreateKaryawan } from "./Karyawan/Create";
export { default as CreateCutiTahunan } from "./MasterData/CutiTahunan/Create";
export { default as CreateHariLiburNasional } from "./MasterData/HariLiburNasional/Create";
export { default as CreateToleransiKeterlambatan } from "./MasterData/ToleransiKeterlambatan/Create";
export { default as CreatePengguna } from "./Pengguna/Create";
