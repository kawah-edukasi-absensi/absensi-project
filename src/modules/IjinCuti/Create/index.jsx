import React, { useRef, useState } from "react";
import { useCurrentRoutes } from "@/utils/hooks";
import * as COMPONENT from "@/components";

export default function CreateIjinCuti() {
  const routes = useCurrentRoutes();
  const formRef = useRef(null);
  const [tipeIjin, setTipeIjin] = useState("");
  const [formData, setFormData] = useState({});
  const [formErrors, setFormErrors] = useState({});
  const [selectValue, setSelectValue] = useState("");

  const options = [
    { label: "Pilih Salah Satu", value: "" },
    { label: "Half Day", value: "Half Day" },
    { label: "Full Day", value: "Full Day" },
  ];

  const handleInputChange = (event) => {
    const { name, value } = event.target;

    if (name === "tipeIjin") {
      setTipeIjin(value);
      setSelectValue(value);
    }
    setFormData({ ...formData, [name]: value });
    setFormErrors({ ...formErrors, [name]: "" });
  };

  const validateForm = () => {
    const errors = {};
    let isValid = true;

    if (!formData.namaPengajuan) {
      errors.namaPengajuan = "Nama Lengkap wajib diisi";
      isValid = false;
    }
    if (!formData.tipeIjin) {
      errors.tipeIjin = "Tipe Ijin wajib diisi";
      isValid = false;
    }
    if (!formData.tanggalMulai) {
      errors.tanggalMulai = "Tanggal Mulai wajib diisi";
      isValid = false;
    }
    if (!formData.jamMulai) {
      errors.jamMulai = "Jam Mulai wajib diisi";
      isValid = false;
      if (tipeIjin === "Full Day") {
        isValid = true;
      }
    }
    if (!formData.tanggalAkhir) {
      errors.tanggalAkhir = "Tanggal Akhir wajib diisi";
      isValid = false;
      if (tipeIjin === "Half Day") {
        isValid = true;
      }
    }
    if (!formData.keterangan) {
      errors.keterangan = "Keterangan wajib diisi";
      isValid = false;
    }

    setFormErrors(errors);
    return isValid;
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    const isValid = validateForm();
    if (isValid) {
      console.log(formData);
      setFormData({});
      setFormErrors({});
      setTipeIjin("");
      setSelectValue("");
      setTimeout(() => {
        formRef.current.reset();
      }, 0);
    }
  };

  return (
    <COMPONENT.PageContainer>
      <COMPONENT.RoutesBread routes={routes} routesName={"Dashboard"} />
      <div className="w-full rounded-md bg-white p-6">
        <COMPONENT.CreateHeader name="Ijin / Cuti" />
        <form
          ref={formRef}
          onSubmit={handleSubmit}
          className="flex flex-col gap-5"
        >
          <div className="flex flex-col">
            <label htmlFor="namaPengajuan" className="w-fit mb-1">
              Nama Pengajuan<span className="text-red-600 font-bold">*</span>
            </label>
            {formErrors.namaPengajuan && (
              <span className="text-red-600 text-sm font-semibold">
                {formErrors.namaPengajuan}
              </span>
            )}
            <input
              type="text"
              autoComplete="nope"
              name="namaPengajuan"
              id="namaPengajuan"
              placeholder="Nama Pengajuan"
              onChange={handleInputChange}
              className="md:w-1/2 border p-3 rounded"
            />
          </div>
          <COMPONENT.SelectInput
            id="tipeIjin"
            name="Tipe Ijin"
            value={selectValue}
            onChange={handleInputChange}
            options={options}
            error={formErrors.tipeIjin}
          />
          <div className="md:w-1/2 flex flex-wrap md:flex-nowrap items-center gap-5">
            <div className="w-fit flex flex-col">
              <label htmlFor="tanggalMulai" className="w-fit mb-1">
                Tanggal Mulai<span className="text-red-600 font-bold">*</span>
              </label>
              {formErrors.tanggalMulai && (
                <span className="text-red-600 text-sm font-semibold">
                  {formErrors.tanggalMulai}
                </span>
              )}
              <input
                type="date"
                name="tanggalMulai"
                id="tanggalMulai"
                onChange={handleInputChange}
                className="border p-3 rounded bg-white"
              />
            </div>
            {tipeIjin === "Half Day" && (
              <div className="w-fit flex flex-col">
                <label htmlFor="jamMulai" className="w-fit mb-1">
                  Jam Mulai<span className="text-red-600 font-bold">*</span>
                </label>
                {formErrors.jamMulai && (
                  <span className="text-red-600 text-sm font-semibold">
                    {formErrors.jamMulai}
                  </span>
                )}
                <input
                  type="time"
                  name="jamMulai"
                  id="jamMulai"
                  onChange={handleInputChange}
                  className="border p-3 rounded bg-white"
                />
              </div>
            )}
            {tipeIjin === "Full Day" && (
              <div className="w-fit flex flex-col">
                <label htmlFor="tanggalAkhir" className="w-fit mb-1">
                  Tanggal Akhir<span className="text-red-600 font-bold">*</span>
                </label>
                {formErrors.tanggalAkhir && (
                  <span className="text-red-600 text-sm font-semibold">
                    {formErrors.tanggalAkhir}
                  </span>
                )}
                <input
                  type="date"
                  name="tanggalAkhir"
                  id="tanggalAkhir"
                  onChange={handleInputChange}
                  className="border p-3 rounded bg-white"
                />
              </div>
            )}
          </div>
          <div className="md:w-1/2 flex flex-col">
            <label htmlFor="keterangan" className="w-fit mb-1">
              Keterangan
              <span className="text-red-600 font-bold">*</span>
            </label>
            {formErrors.keterangan && (
              <span className="text-red-600 text-sm font-semibold">
                {formErrors.keterangan}
              </span>
            )}
            <textarea
              autoComplete="nope"
              name="keterangan"
              id="keterangan"
              placeholder="Keterangan"
              cols="30"
              rows="5"
              onChange={handleInputChange}
              className="border p-3 rounded"
            />
          </div>
          <COMPONENT.ButtonForm name="Ajukan" />
        </form>
      </div>
    </COMPONENT.PageContainer>
  );
}
