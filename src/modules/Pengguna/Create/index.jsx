import React, { useRef, useState } from "react";
import { useNavigate } from "react-router-dom";
import { useCurrentRoutes } from "@/utils/hooks";
import * as COMPONENT from "@/components";

export default function CreatePengguna() {
  const navigate = useNavigate();
  const routes = useCurrentRoutes();
  const formRef = useRef(null);
  const [formData, setFormData] = useState({});
  const [formErrors, setFormErrors] = useState({});
  const [selectValue, setSelectValue] = useState("");

  const options = [
    { label: "Pilih Salah Satu", value: "" },
    { label: "Admin", value: "Admin" },
    { label: "User", value: "User" },
  ];

  const handleInputChange = (event) => {
    const { name, value } = event.target;

    name === "role" && setSelectValue(value);
    setFormData({ ...formData, [name]: value });
    setFormErrors({ ...formErrors, [name]: "" });
  };

  const validateForm = () => {
    const errors = {};
    let isValid = true;

    if (!formData.namaLengkap) {
      errors.namaLengkap = "Nama Lengkap wajib diisi";
      isValid = false;
    }
    if (!formData.tempatLahir) {
      errors.tempatLahir = "Tempat Lahir wajib diisi";
      isValid = false;
    }
    if (!formData.tanggalLahir) {
      errors.tanggalLahir = "Tanggal Lahir wajib diisi";
      isValid = false;
    }
    if (!formData.email) {
      errors.email = "Alamat Email wajib diisi";
      isValid = false;
    } else if (
      !/^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/.test(formData.email)
    ) {
      errors.email = "Format Email Salah";
      isValid = false;
    }
    if (!formData.role) {
      errors.role = "Role wajib diisi";
      isValid = false;
    }

    setFormErrors(errors);
    return isValid;
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    const isValid = validateForm();
    if (isValid) {
      console.log(formData);
      setFormData({});
      setFormErrors({});
      setSelectValue("");
      setTimeout(() => {
        formRef.current.reset();
      }, 0);
    }
  };

  return (
    <COMPONENT.PageContainer>
      <COMPONENT.RoutesBread routes={routes} routesName={"Dashboard Create"} />
      <COMPONENT.CreateHeader navigate={() => navigate(-1)} />
      <div className="w-full rounded-md bg-white p-6">
        <form
          ref={formRef}
          onSubmit={handleSubmit}
          className="flex flex-col gap-5"
        >
          <div className="flex flex-col">
            <label htmlFor="namaLengkap" className="w-fit mb-1">
              Nama Lengkap<span className="text-red-600 font-bold">*</span>
            </label>
            {formErrors.namaLengkap && (
              <span className="text-red-600 text-sm font-semibold">
                {formErrors.namaLengkap}
              </span>
            )}
            <input
              type="text"
              autoComplete="nope"
              name="namaLengkap"
              id="namaLengkap"
              placeholder="Nama Lengkap"
              onChange={handleInputChange}
              className="md:w-1/2 border p-3 rounded"
            />
          </div>
          <div className="flex flex-wrap gap-5">
            <div className="w-full md:w-fit flex flex-col">
              <label htmlFor="tempatLahir" className="w-fit mb-1">
                Tempat Lahir<span className="text-red-600 font-bold">*</span>
              </label>
              {formErrors.tempatLahir && (
                <span className="text-red-600 text-sm font-semibold">
                  {formErrors.tempatLahir}
                </span>
              )}
              <input
                type="text"
                autoComplete="nope"
                name="tempatLahir"
                id="tempatLahir"
                placeholder="Tempat Lahir"
                onChange={handleInputChange}
                className="border p-3 rounded"
              />
            </div>
            <div className="w-fit flex flex-col">
              <label htmlFor="tanggalLahir" className="w-fit mb-1">
                Tanggal Lahir<span className="text-red-600 font-bold">*</span>
              </label>
              {formErrors.tanggalLahir && (
                <span className="text-red-600 text-sm font-semibold">
                  {formErrors.tanggalLahir}
                </span>
              )}
              <input
                type="date"
                name="tanggalLahir"
                id="tanggalLahir"
                onChange={handleInputChange}
                className="border p-3 rounded bg-white"
              />
            </div>
          </div>
          <div className="flex flex-col">
            <label htmlFor="email" className="w-fit mb-1">
              Email<span className="text-red-600 font-bold">*</span>
            </label>
            {formErrors.email && (
              <span className="text-red-600 text-sm font-semibold">
                {formErrors.email}
              </span>
            )}
            <input
              autoComplete="nope"
              type="email"
              name="email"
              id="email"
              placeholder="Email"
              onChange={handleInputChange}
              className="md:w-1/2 border p-3 rounded"
            />
          </div>
          <COMPONENT.SelectInput
            id="role"
            name="Role"
            value={selectValue}
            onChange={handleInputChange}
            options={options}
            error={formErrors.role}
          />
          <COMPONENT.ButtonForm />
        </form>
      </div>
    </COMPONENT.PageContainer>
  );
}
