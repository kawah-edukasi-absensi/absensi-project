import React from "react";
import { useCurrentRoutes } from "@/utils/hooks";
import { pengguna } from "@/mocks/fakeData";
import { ROUTE } from "@/lib/Routes";
import * as COMPONENT from "@/components";

export default function Pengguna() {
  const routes = useCurrentRoutes();

  return (
    <COMPONENT.PageContainer>
      <div className="w-full flex flex-col">
        <COMPONENT.RoutesBread routes={routes} routesName={"Dashboard"} />
        <COMPONENT.Table
          dataTable={pengguna}
          muatUlang={ROUTE.PENGGUNA}
          create={ROUTE.CREATE_PENGGUNA}
        />
      </div>
    </COMPONENT.PageContainer>
  );
}
