import React from "react";

export default function RoutesBread({ routes, routesName }) {
  return (
    <>
      <h1 className="md:text-2xl text-lg font-bold text-slate-600 py-3 mb-4">
        {routesName === "Dashboard" && (
          <>
            <span className="text-slate-400 font-normal">Dashboard / </span>
            {routes[0].name}
          </>
        )}
        {routesName === "Dashboard Create" && (
          <>
            <span className="text-slate-400 font-normal">
              Dashboard / {routes[0].name} /{" "}
            </span>
            {routes[1].name} {routes[2] && `/ ${routes[2].name}`}
          </>
        )}
        {routesName === "Absensi" && (
          <>
            <span className="text-slate-400 font-normal">
              {routes[0].name} /{" "}
            </span>
            {routes[1].name} {routes[2] && `/ ${routes[2].name}`}
          </>
        )}
      </h1>
    </>
  );
}
