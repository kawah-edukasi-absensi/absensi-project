/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect, useMemo, useState } from "react";
import {
  createColumnHelper,
  flexRender,
  getCoreRowModel,
  useReactTable,
} from "@tanstack/react-table";
import { useCamelToNormalCase } from "@/utils/hooks";
import { NavLink } from "react-router-dom";
import { BiListPlus, BiRefresh, BiSearchAlt2 } from "react-icons/bi";

/**
 * Define your own custom columns from columnsTable props
 */

export default function Table(props) {
  const { dataTable, columnsTable, muatUlang, create } = props;
  const columnHelper = createColumnHelper();
  const camelToNormalCase = useCamelToNormalCase();
  const [query, setQuery] = useState("");
  const [filtered, setFiltered] = useState([]);

  useEffect(() => {
    if (query === "") {
      setFiltered(dataTable);
    }
  }, [dataTable, query]);

  const filteredItems = useMemo(() => {
    return dataTable.filter((item) => {
      return Object.values(item).some((value) => {
        return (
          typeof value === "string" &&
          value.toLowerCase().includes(query.toLowerCase())
        );
      });
    });
  }, [dataTable, query]);

  const handleSearchSubmit = (e) => {
    e.preventDefault();
    setFiltered(filteredItems);
  };

  const dataSample = dataTable.length > 0 ? dataTable[0] : {};

  const columnsMap = [
    ...Object.keys(dataSample).map((item) =>
      columnHelper.accessor(item, {
        header: () => {
          const ilc = item.toLowerCase();
          if ((ilc === "id") | (ilc === "nik") | (ilc === "userid")) {
            return <h1>{item.toUpperCase()}</h1>;
          } else {
            return <h1>{camelToNormalCase(item)}</h1>;
          }
        },
      })
    ),
  ];

  const data = filtered.length > 0 ? filtered : filteredItems;

  const table = useReactTable({
    data,
    columns: columnsTable ?? columnsMap,
    getCoreRowModel: getCoreRowModel(),
  });

  const totalColumns = table
    .getHeaderGroups()
    .reduce((count, headerGroup) => count + headerGroup.headers.length, 0);

  return (
    <div className="w-full flex flex-col bg-white px-5 rounded-lg shadow-lg">
      <div
        id="table-data-handler"
        className="w-full flex justify-end items-center py-4 md:gap-4 gap-2 border-b overflow-x-auto"
      >
        <NavLink
          to={muatUlang}
          className="py-3 md:px-6 px-4 text-sm flex items-center gap-3 bg-slate-600 hover:bg-slate-500 rounded-md text-white mr-auto whitespace-nowrap"
        >
          <div className="text-lg">
            <BiRefresh />
          </div>
          <div className="hidden md:flex">Muat Ulang</div>
        </NavLink>
        <form onSubmit={handleSearchSubmit} className="flex">
          <input
            value={query}
            type="search"
            name="cariData"
            id="cariData"
            placeholder="Search"
            className="md:w-full w-[6.5rem] border md:py-2.5 py-2 px-4 rounded-l-md"
            onChange={(e) => setQuery(e.target.value)}
          />
          <button
            type="submit"
            className="rounded-r-md bg-slate-600 hover:bg-slate-500 py-3.5 px-4 text-white"
          >
            <BiSearchAlt2 />
          </button>
        </form>
        <NavLink
          to={create}
          className="py-3 md:px-8 px-4 text-sm flex items-center gap-3 bg-green-500 hover:bg-green-400 rounded-md text-white"
        >
          <div className="text-lg">
            <BiListPlus />
          </div>
          <div className="hidden md:flex">Tambah</div>
        </NavLink>
      </div>

      <div className="flex flex-col rounded-lg">
        <div className="overflow-x-auto sm:-mx-6 lg:-mx-8">
          <div className="py-2 inline-block min-w-full sm:px-6 lg:px-8">
            <div className="overflow-hidden">
              <table className="min-w-full">
                <thead className="bg-white border-b">
                  {table.getHeaderGroups().map((headerGroup) => (
                    <tr key={headerGroup.id}>
                      {headerGroup.headers.map((header) => (
                        <th
                          key={header.id}
                          scope="col"
                          className="text-sm font-medium text-gray-900 px-6 py-4 text-left"
                        >
                          {header.isPlaceholder
                            ? null
                            : flexRender(
                                header.column.columnDef.header,
                                header.getContext()
                              )}
                        </th>
                      ))}
                    </tr>
                  ))}
                </thead>
                <tbody>
                  {table.getRowModel().rows.length > 0 ? (
                    table.getRowModel().rows.map((row) => (
                      <tr
                        key={row.id}
                        className="bg-white border-b transition duration-300 ease-in-out hover:bg-gray-100"
                      >
                        {row.getVisibleCells().map((cell) => (
                          <td
                            key={cell.id}
                            className="text-sm text-gray-900 font-light px-6 py-4 whitespace-nowrap"
                          >
                            {flexRender(
                              cell.column.columnDef.cell,
                              cell.getContext()
                            )}
                          </td>
                        ))}
                      </tr>
                    ))
                  ) : (
                    <tr>
                      <td colSpan={totalColumns} className="text-center py-4">
                        No data found.
                      </td>
                    </tr>
                  )}
                </tbody>
                <tfoot>
                  {table.getFooterGroups().map((footerGroup) => (
                    <tr key={footerGroup.id}>
                      {footerGroup.headers.map((header) => (
                        <th key={header.id}>
                          {header.isPlaceholder
                            ? null
                            : flexRender(
                                header.column.columnDef.footer,
                                header.getContext()
                              )}
                        </th>
                      ))}
                    </tr>
                  ))}
                </tfoot>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
