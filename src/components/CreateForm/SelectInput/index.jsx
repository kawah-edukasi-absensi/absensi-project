import React from "react";

export default function SelectInput({
  id,
  name,
  value,
  onChange,
  options,
  error,
}) {
  return (
    <div className="flex flex-col">
      <label htmlFor={id}>
        {name}
        <span className="text-red-600 font-bold">*</span>
      </label>
      {error && <p className="text-red-600 text-sm font-semibold">{error}</p>}
      <select
        id={id}
        name={id}
        value={value}
        onChange={onChange}
        className="md:w-1/2 border p-3 rounded bg-white"
      >
        {options.map((option) => (
          <option key={option.value} value={option.value}>
            {option.label}
          </option>
        ))}
      </select>
    </div>
  );
}
