import React from "react";

export default function SelectInputKotaKabupaten({
  id,
  name,
  value,
  onChange,
  options,
  error,
}) {
  return (
    <div className="w-full flex flex-col md:row-start-2 md:row-end-3">
      <label htmlFor={id}>
        {name}
        <span className="text-red-600 font-bold">*</span>
      </label>
      {error && <p className="text-red-600 text-sm font-semibold">{error}</p>}
      <select
        id={id}
        name={id}
        value={value}
        onChange={onChange}
        className="border p-3 rounded bg-white"
      >
        {options.map((option) => (
          <option key={option.value} value={option.value}>
            {option.label}
          </option>
        ))}
      </select>
    </div>
  );
}
