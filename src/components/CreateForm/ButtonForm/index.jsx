import React from "react";

export default function ButtonForm({ name }) {
  return (
    <div className="w-full flex md:justify-end justify-center items-center gap-4 mt-10">
      <button className="border-2 border-green-500 hover:border-green-400 bg-green-500 hover:bg-green-400 text-white py-3 px-16 rounded-md text-sm">
        {name || "Simpan Data"}
      </button>
    </div>
  );
}
