import React from "react";
import { BiArrowBack } from "react-icons/bi";

export default function CreateHeader(props) {
  const { name, navigate } = props;

  return (
    <>
      {name === "Ijin / Cuti" || name === "Remote" ? (
        <h1 className="text-2xl font-bold text-slate-600 mb-6">{name}</h1>
      ) : (
        <div className="flex items-center gap-4 mb-4 py-4 border-t-2 border-b-2">
          <button
            onClick={navigate}
            className="md:px-5 md:py-2.5 py-2 px-3 text-sm flex items-center md:gap-3 gap-2 bg-slate-600 hover:bg-slate-500 rounded-md text-white whitespace-nowrap"
          >
            <BiArrowBack />
            Kembail
          </button>
          <h2 className="text-lg text-slate-600 font-semibold">Tambah Data</h2>
        </div>
      )}
    </>
  );
}
